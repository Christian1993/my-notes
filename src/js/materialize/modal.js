import M from 'materialize-css/dist/js/materialize.min.js'

export const modalInit = function () {
  const elems = document.querySelectorAll('.modal'); // eslint-disable-next-line
  const instances = M.Modal.init(elems);
}
