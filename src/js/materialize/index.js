import {
  navbar
} from './navbar';

// for global components
export const materializeInit = () => {
  navbar();
}
