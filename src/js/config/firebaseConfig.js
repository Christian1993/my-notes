import firebase from "firebase/app";
import "firebase/app";
import "firebase/firestore";
import "firebase/auth";

const config = {
  apiKey: "AIzaSyAIQrxwVDWZetkAtd4D-sDyHxJckciPC5Y",
  authDomain: "my-notes-77090.firebaseapp.com",
  databaseURL: "https://my-notes-77090.firebaseio.com",
  projectId: "my-notes-77090",
  storageBucket: "my-notes-77090.appspot.com",
  messagingSenderId: "236074352180"
};

firebase.initializeApp(config);
firebase.firestore();
export default firebase;
