import {
  SIGNIN_SUCCESS,
  SIGNIN_ERROR,
  SIGNOUT_SUCCESS,
  SIGNUP_SUCCESS,
  SIGNUP_ERROR,
  DELETE_ACCOUNT
} from "../actions/types";

export const signIn = credentials => (dispatch, getState, { getFirebase }) => {
  const firebase = getFirebase();
  firebase
    .auth()
    .signInWithEmailAndPassword(credentials.email, credentials.password)
    .then(() => {
      dispatch({
        type: SIGNIN_SUCCESS
      });
    })
    .catch(err => {
      dispatch({
        type: SIGNIN_ERROR,
        payload: err.message
      });
    });
};

export const signOut = () => (dispatch, getState, { getFirebase }) => {
  const firebase = getFirebase();
  firebase
    .auth()
    .signOut()
    .then(() => {
      dispatch({
        type: SIGNOUT_SUCCESS
      });
    });
};

export const signUp = user => (
  dispatch,
  getState,
  { getFirebase, getFirestore }
) => {
  const firebase = getFirebase();
  const firestore = getFirestore();
  firebase
    .auth()
    .createUserWithEmailAndPassword(user.email, user.password)
    .then(res => {
      return firestore
        .collection("users")
        .doc(res.user.uid)
        .set({
          firstName: user.firstName
        });
    })
    .then(() => dispatch({ type: SIGNUP_SUCCESS }))
    .catch(err => dispatch({ type: SIGNUP_ERROR, payload: err.message }));
};

export const deleteAccount = id => (
  dispatch,
  getState,
  { getFirebase, getFirestore }
) => {
  const firebase = getFirebase();
  const firestore = getFirestore();
  const user = firebase.auth().currentUser;
  firestore
    .collection("notes")
    .where("authorId", "==", id)
    .get()
    .then(querySnapshot => {
      const batch = firestore.batch();
      querySnapshot.forEach(doc => batch.delete(doc.ref));
      return batch.commit();
    })
    .then(() => {
      user.delete().then(() =>
        dispatch({
          type: DELETE_ACCOUNT
        })
      );
    });
};
