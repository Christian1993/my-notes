import {
  ADD_NOTE_SUCCESS,
  ADD_NOTE_ERROR,
  DELETE_NOTE_SUCCESS,
  DELETE_NOTE_ERROR,
  FILTER
} from "../actions/types";

export const addNote = note => (
  dispatch,
  getState,
  { getFirebase, getFirestore }
) => {
  const firestore = getFirestore();
  const uid = getState().firebase.auth.uid;
  firestore
    .collection("notes")
    .add({ ...note, authorId: uid })
    .then(() =>
      dispatch({
        type: ADD_NOTE_SUCCESS
      })
    )
    .catch(err =>
      dispatch({
        type: ADD_NOTE_ERROR,
        payload: err.message
      })
    );
};

export const deleteNote = id => (
  dispatch,
  getState,
  { getFirebase, getFirestore }
) => {
  const firestore = getFirestore();
  firestore
    .collection("notes")
    .doc(id)
    .delete()
    .then(() =>
      dispatch({
        type: DELETE_NOTE_SUCCESS
      })
    )
    .catch(err =>
      dispatch({
        type: DELETE_NOTE_ERROR,
        payload: err.message
      })
    );
};

export const filter = data => dispatch =>
  dispatch({ type: FILTER, payload: data });
