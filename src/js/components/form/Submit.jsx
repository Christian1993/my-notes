import React, { Component } from "react";

export default class Submit extends Component {
  render() {
    const { classes, icon, text } = this.props.config;
    const iconMarkup = icon ? (
      <i className="material-icons right">{icon}</i>
    ) : null;
    return (
      <button className={classes} type="submit">
        {text}
        {iconMarkup}
      </button>
    );
  }
}
