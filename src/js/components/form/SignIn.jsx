import React, { Component } from "react";
import { connect } from "react-redux";

import Input from "./Input";
import Submit from "./Submit";
import { signIn } from "../../actions/authActions";

class SignIn extends Component {
  state = {
    email: "",
    password: ""
  };

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  submit = e => {
    e.preventDefault();
    this.props.signIn(this.state);
  };

  render() {
    const { authError } = this.props;
    return (
      <form className="form" id="signUp" onSubmit={this.submit}>
        <div className="row">
          <Input
            config={{
              onChange: this.onChange,
              wrapperClass: "form",
              label: "Email",
              name: "email",
              type: "email",
              value: this.state.email,
              icon: "email"
            }}
          />
          <Input
            config={{
              onChange: this.onChange,
              wrapperClass: "form",
              label: "Password",
              name: "password",
              type: "password",
              value: this.state.password,
              icon: "vpn_key"
            }}
          />
        </div>
        <div className="row">
          <div className="col s12 m6 form__submit-wrapper">
            <Submit
              config={{
                classes: "btn-large waves-effect waves-light",
                text: "Submit",
                icon: "send"
              }}
            />
          </div>
          <div className="col s12 m6 form__error">
            <span className="red-text error">{authError}</span>
          </div>
        </div>
      </form>
    );
  }
}

const mapStateToProps = state => {
  return {
    authError: state.auth.authError
  };
};

export default connect(
  mapStateToProps,
  { signIn }
)(SignIn);
