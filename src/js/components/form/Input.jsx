import React, { Component } from "react";

export default class Input extends Component {
  render() {
    const {
      width = "s12",
      icon,
      label,
      wrapperClass,
      name,
      type,
      onChange,
      value = "",
      min= null,
      max = null
    } = this.props.config;
    const iconMarkup = icon ? (
      <i className="material-icons prefix">{icon}</i>
    ) : null;
    return (
      <div className={`input-field ${wrapperClass}__wrapper col ${width}`}>
        {iconMarkup}
        <input
          id={name}
          type={type}
          className={`validate ${wrapperClass}__input`}
          name={name}
          onChange={onChange}
          value={value}
          min={min}
          max={max}
        />
        <label className={`${wrapperClass}__label`} htmlFor={name}>
          {label}
        </label>
      </div>
    );
  }
}
