import React, { Component } from "react";

export default class MessageBox extends Component {
  render() {
    const { message } = this.props;
    return (
      <div className="message">
        <div className="message__box">
          <p className="message__text">{message}</p>
        </div>
      </div>
    );
  }
}
