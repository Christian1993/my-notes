import React, { Component } from "react";

import { connect } from "react-redux";
import { compose } from "redux";
import { firestoreConnect } from "react-redux-firebase";
import Note from "./notes/Note";

class Board extends Component {
  render() {
    const { notes, filterData } = this.props;
    const filtered =
      filterData.length > 0
        ? notes && notes.filter(note => note.category.includes(filterData))
        : notes;
    return (
      <div className="board">
        <div className="row">
          {filtered && filtered.map(note => <Note note={note} key={note.id} />)}
        </div>
      </div>
    );
  }
}

const mapstateToProps = state => {
  return {
    auth: state.firebase.auth,
    notes: state.firestore.ordered.notes,
    filterData: state.note.filter
  };
};

export default compose(
  connect(mapstateToProps),
  firestoreConnect(props => [
    {
      collection: "notes",
      storeAs: "notes",
      where: [["authorId", "==", props.auth.uid]]
    }
  ])
)(Board);
