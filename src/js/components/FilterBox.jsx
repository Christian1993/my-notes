import React, { Component } from "react";
import { connect } from "react-redux";

import Input from "./form/Input";
import { filter } from "../actions/noteActions";

class FilterBox extends Component {
  handleChange = e => {
    this.props.filter(e.target.value);
  };
  render() {
    const filterData = this.props.filterData;
    return (
      <div className="filter-box">
        <h3 className="filter-box__title">Filter your notes</h3>
        <Input
          config={{
            onChange: this.handleChange,
            wrapperClass: "filter-box",
            label: "Category",
            name: "filter",
            type: "text",
            value: filterData
          }}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    filterData: state.note.filter
  };
};

export default connect(
  mapStateToProps,
  { filter }
)(FilterBox);
