import React, { Component } from "react";

export default class Panel extends Component {
  render() {
    return (
      <div className="panel">
        <div className="row">
          <div className="col s12">
            <button
              className="panel__button waves-effect waves-light btn modal-trigger"
              data-target="add-text-note"
            >
              Add note
            </button>
            <button
              className="panel__button waves-effect waves-light btn modal-trigger"
              data-target="add-list-note"
            >
              Add time-list
            </button>
          </div>
        </div>
      </div>
    );
  }
}
