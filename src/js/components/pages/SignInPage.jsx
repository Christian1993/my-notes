import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import MessageBox from "../MessageBox";
import SignIn from "../form/SignIn";

class SignInPage extends Component {
  render() {
    const auth = this.props.auth;
    if (auth.uid) return <Redirect to="/" />;
    return (
      <main className="main">
        <section className="signup">
          <div className="container">
            <div className="row">
              <div className="col s12">
                <MessageBox message="Hello, please sign in. Happy planning!" />
              </div>
            </div>
            <SignIn />
          </div>
        </section>
      </main>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.firebase.auth
  };
};

export default connect(mapStateToProps)(SignInPage);
