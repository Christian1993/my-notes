import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import Panel from "../Panel";
import Board from "../Board";
import AddTextNote from "../notes/AddTextNote";
import AddListNote from "../notes/AddListNote";
import FilterBox from "../FilterBox";

class Home extends Component {
  render() {
    const auth = this.props.auth;
    if (!auth.uid) return <Redirect to="/signin" />;
    return (
      <main className="main">
        <section className="intro">
          <div className="container">
            <div className="row">
              <div className="col s12 m3">
                <Panel />
              </div>
              <div className="col s12 m9">
                <FilterBox />
              </div>
            </div>
            <div className="row">
              <div className="col s12">
                <Board />
              </div>
            </div>
          </div>
        </section>
        <AddTextNote />
        <AddListNote />
      </main>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.firebase.auth
  };
};

export default connect(mapStateToProps)(Home);
