import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import MessageBox from "../MessageBox";
import SignUp from "../form/SignUp";

class SignUpPage extends Component {
  render() {
    const auth = this.props.auth;
    if (auth.uid) return <Redirect to="/" />;
    return (
      <main className="main">
        <section className="signup">
          <div className="container">
            <div className="row">
              <div className="col s12">
                <MessageBox message="Hello new User! Sign up and start to create your own notes." />
              </div>
            </div>
            <SignUp />
          </div>
        </section>
      </main>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.firebase.auth
  };
};

export default connect(mapStateToProps)(SignUpPage);
