import React, { Component } from "react";
import { Redirect } from "react-router-dom";

import AccountTitle from "../account/AccountTitle";
import AccountControls from "../account/AccountControls";
import { connect } from "react-redux";

class Account extends Component {
  render() {
    const uid = this.props.auth.uid;

    if (!uid) return <Redirect to="/signin" />;
    return (
      <main className="main">
        <section className="account">
          <div className="container">
            <AccountTitle />
            <AccountControls />
          </div>
        </section>
      </main>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.firebase.auth
  };
};

export default connect(mapStateToProps)(Account);
