import React, { Component } from "react";

export default class TextContent extends Component {
  renderData = data => {
    if (data.length === 0) return null;
    if (data.length === 1) return <p className="note__data">{data}</p>;
    return (
      <ul className="note__list browser-default">
        {data.map((item, index) => (
          <li className="note__item" key={index}>
            {item}
          </li>
        ))}
      </ul>
    );
  };
  render() {
    const { data } = this.props.note;
    return (
      <div className="row">
        <div className="note__content">{this.renderData(data)}</div>
      </div>
    );
  }
}
