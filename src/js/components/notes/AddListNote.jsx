import React, { Component } from "react";
import { connect } from "react-redux";

import { addNote } from "../../actions/noteActions";
import { modalInit } from "../../materialize/modal";
import Input from "../form/Input";

class AddListNote extends Component {
  state = {
    type: "list",
    title: "",
    category: "",
    data: [
      {
        data: "",
        hour: "",
        minute: ""
      }
    ]
  };

  componentDidMount() {
    modalInit();
  }

  addInput = () => {
    const length = this.state.data.length;
    const newData = [...this.state.data];
    if (length < 10) {
      newData.push({
        data: "",
        hour: "",
        minute: ""
      });
      this.setState({
        data: newData
      });
    }
  };

  deleteInput = () => {
    const length = this.state.data.length;
    if (length > 1) {
      const newData = [...this.state.data.slice(0, length - 1)];
      this.setState({
        data: newData
      });
    }
  };

  clearForm = () => {
    const config = {
      title: "",
      category: "",
      data: [
        {
          data: "",
          hour: "",
          minute: ""
        }
      ]
    };
    this.setState({
      ...config
    });
  };

  handleDataChange = e => {
    const dashIndex = e.target.id.indexOf("-");
    const index = e.target.id.slice(dashIndex + 1);
    const identifier = e.target.id.slice(0, dashIndex);
    const value = e.target.value;
    const length = this.state.data.length;

    const newData = [...this.state.data];

    if (index < length) {
      const newItem = { ...newData[index], [identifier]: value };
      newData[index] = newItem;
      this.setState({ data: newData });
    }
  };

  handleChange = e => {
    const dashIndex = e.target.id.indexOf("-");
    const identifier = e.target.id.slice(dashIndex + 1);
    this.setState({
      [identifier]: e.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.addNote(this.state);
    this.clearForm();
  };

  render() {
    const config = {
      onChange: this.handleDataChange,
      wrapperClass: "modal",
      type: "text"
    };
    return (
      <div id="add-list-note" className="modal">
        <div className="container">
          <div className="row">
            <div className="col s12">
              <div className="modal__header">
                <h4>Add new note</h4>
              </div>
            </div>
          </div>

          <div className="row">
            <form id="add-list-note-form" onSubmit={this.handleSubmit}>
              <div className="modal__content">
                <Input
                  config={{
                    ...config,
                    value: this.state.title,
                    onChange: this.handleChange,
                    name: "list-title",
                    label: "Title",
                    width: "s12 m6"
                  }}
                />
                <Input
                  config={{
                    ...config,
                    value: this.state.category,
                    onChange: this.handleChange,
                    name: "list-category",
                    label: "Category",
                    width: "s12 m6"
                  }}
                />
                {this.state.data.map((input, index) => (
                  <div key={index}>
                    <Input
                      config={{
                        ...config,
                        type: "number",
                        name: `hour-${index}`,
                        value: this.state.data[index].hour,
                        min: "0",
                        max: "23",
                        width: "s2"
                      }}
                    />
                    <Input
                      config={{
                        ...config,
                        type: "number",
                        name: `minute-${index}`,
                        value: this.state.data[index].minute,
                        min: "0",
                        max: "59",
                        width: "s2"
                      }}
                    />
                    <Input
                      config={{
                        ...config,
                        name: `data-${index}`,
                        value: this.state.data[index].data,
                        width: "s8"
                      }}
                    />
                  </div>
                ))}
              </div>
            </form>
          </div>
          <div className="row">
            <div className="modal__footer">
              <button
                className="modal-close waves-effect waves-green btn"
                type="submit"
                form="add-list-note-form"
              >
                Save
              </button>
              <button
                className="btn-floating modal__add"
                onClick={this.addInput}
              >
                <i className="material-icons">add</i>
              </button>

              <button
                className="btn-floating modal__delete"
                onClick={this.deleteInput}
              >
                <i className="material-icons">delete</i>
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  null,
  { addNote }
)(AddListNote);
