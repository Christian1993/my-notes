import React, { Component } from "react";
import { connect } from "react-redux";

import { addNote } from "../../actions/noteActions";
import { modalInit } from "../../materialize/modal";
import Input from "../form/Input";

class AddTextNote extends Component {
  state = {
    type: "text",
    title: "",
    category: "",
    data: [""]
  };
  
  componentDidMount() {
    modalInit();
  }

  addInput = () => {
    const length = this.state.data.length;
    const newData = [...this.state.data];
    if (length < 10) {
      newData.push("");
      this.setState({
        data: newData
      });
    }
  };

  deleteInput = () => {
    const length = this.state.data.length;
    if (length > 1) {
      const newData = [...this.state.data.slice(0, length - 1)];
      this.setState({
        data: newData
      });
    }
  };

  clearForm = () => {
    const config = {
      title: "",
      category: "",
      data: [""]
    };
    this.setState({
      ...config
    });
  };

  handleDataChange = e => {
    const index = e.target.id;
    const value = e.target.value;
    const length = this.state.data.length;
    const newData = [...this.state.data];
    if (index < length) {
      newData[index] = value;
      this.setState({ data: newData });
    }
  };
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.addNote(this.state);
    this.clearForm();
  };

  render() {
    const config = {
      onChange: this.handleDataChange,
      wrapperClass: "modal",
      type: "text"
    };
    return (
      <div id="add-text-note" className="modal">
        <div className="container">
          <div className="row">
            <div className="col s12">
              <div className="modal__header">
                <h4>Add new note</h4>
              </div>
            </div>
          </div>

          <div className="row">
            <form id="add-text-note-form" onSubmit={this.handleSubmit}>
              <div className="modal__content">
                <Input
                  config={{
                    ...config,
                    value: this.state.title,
                    onChange: this.handleChange,
                    name: "title",
                    label: "Title",
                    width: "s12 m6"
                  }}
                />
                <Input
                  config={{
                    ...config,
                    value: this.state.category,
                    onChange: this.handleChange,
                    name: "category",
                    label: "Category",
                    width: "s12 m6"
                  }}
                />
                {this.state.data.map((input, index) => (
                  <Input
                    key={index}
                    config={{
                      ...config,
                      name: index,
                      value: this.state.data[index]
                    }}
                  />
                ))}
              </div>
            </form>
          </div>
          <div className="row">
            <div className="modal__footer">
              <button
                className="modal-close waves-effect waves-green btn"
                type="submit"
                form="add-text-note-form"
              >
                Save
              </button>
              <button
                className="btn-floating modal__add"
                onClick={this.addInput}
              >
                <i className="material-icons">add</i>
              </button>

              <button
                className="btn-floating modal__delete"
                onClick={this.deleteInput}
              >
                <i className="material-icons">delete</i>
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  null,
  { addNote }
)(AddTextNote);
