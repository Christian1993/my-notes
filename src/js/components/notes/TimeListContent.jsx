import React, { Component } from "react";

export default class TimeListContent extends Component {
  render() {
    const { data } = this.props.note;
    return (
      <ul className="note__list">
        {data &&
          data.map((note, index) => (
            <li className="note__item note__item--time-list" key={index}>
              <span>
                <b>
                  {note.hour}-{note.minute}:
                </b>
              </span>{" "}
              {note.data}
            </li>
          ))}
      </ul>
    );
  }
}
