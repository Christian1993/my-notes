import React, { Component } from "react";
import { connect } from "react-redux";
import { deleteNote } from "../../actions/noteActions";

class NoteHeader extends Component {
  render() {
    const { title, id } = this.props.note;
    return (
      <div className="note__header">
        <div className="row">
          <p className="note__title">{title}</p>
          <span
            className="note__delete new badge"
            data-badge-caption="X"
            data-id={id}
            onClick={() => this.props.deleteNote(id)}
          />
        </div>
      </div>
    );
  }
}
export default connect(
  null,
  { deleteNote }
)(NoteHeader);
