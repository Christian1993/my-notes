import React, { Component } from "react";

export default class NoteFooter extends Component {
  render() {
    const category = this.props.note.category;
    return (
      <div className="note__footer">
        <p>{category}</p>
      </div>
    );
  }
}
