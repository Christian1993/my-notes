import React, { Component } from "react";

import NoteHeader from "./NoteHeader";
import NoteFooter from './NoteFooter';
import TextContent from "./TextContent";
import TimeListContent from "./TimeListContent";

export default class Note extends Component {
  renderContent = note => {
    switch (note.type) {
      case "text": {
        return <TextContent note={note} />;
      }
      case "list": {
        return <TimeListContent note={note} />;
      }
      default: {
        return null;
      }
    }
  };
  render() {
    const { note } = this.props;
    return (
      <div className="col s12 m6 l4">
        <div className="note note--text clearfix center-align">
          <NoteHeader note={note} />
          {this.renderContent(note)}
          <NoteFooter note={note} />
        </div>
      </div>
    );
  }
}
