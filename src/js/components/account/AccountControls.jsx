import React, { Component } from "react";
import { connect } from "react-redux";
import { deleteAccount } from "../../actions/authActions";

class AccountControls extends Component {
  render() {
    return (
      <div className="row">
        <div className="col s12 m3 l3">
          <button
            className="waves-effect waves-light btn red darken-4"
            onClick={() => this.props.deleteAccount(this.props.uid)}
          >
            <i className="account__icon material-icons left">error_outline</i>
            delete account
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    uid: state.firebase.auth.uid
  };
};

export default connect(
  mapStateToProps,
  { deleteAccount }
)(AccountControls);
