import React, { Component } from "react";

export default class AccountTitle extends Component {
  render() {
    return (
      <div className="row">
        <div className="col s12">
          <h1 className="account__title">Manage your account</h1>
        </div>
      </div>
    );
  }
}
