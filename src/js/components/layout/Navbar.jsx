import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { signOut } from "../../actions/authActions";

class Navbar extends Component {
  render() {
    const unSigned = (
      <>
        <li>
          <NavLink className="navbar__link" to="/signin">
            Sign In
          </NavLink>
        </li>
        <li>
          <NavLink className="navbar__link" to="/signup">
            Sign Up
          </NavLink>
        </li>
      </>
    );

    const signed = (
      <>
        <li>
          <NavLink to="/account">
            <span
              className="navbar__badge new badge"
              data-badge-caption={this.props.firstName}
            />
          </NavLink>
        </li>
        <li>
          {/* eslint-disable jsx-a11y/anchor-is-valid */}
          <a className="navbar__link" onClick={this.props.signOut}>
            Sign Out
          </a>
        </li>
      </>
    );
    const auth = this.props.auth;
    const links = auth.uid ? signed : unSigned;
    return (
      <>
        <nav>
          <div className="nav-wrapper navbar">
            <NavLink
              to="#"
              data-target="mobile-demo"
              className="sidenav-trigger"
            >
              <i className="material-icons">menu</i>
            </NavLink>
            <NavLink to="/" className="navbar__title brand-logo">
              MyNotes
            </NavLink>

            <ul className="right hide-on-med-and-down">{links}</ul>
          </div>
        </nav>

        <ul className="sidenav" id="mobile-demo">
          {links}
        </ul>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.firebase.auth,
    firstName: state.firebase.profile.firstName
  };
};

export default connect(
  mapStateToProps,
  { signOut }
)(Navbar);
