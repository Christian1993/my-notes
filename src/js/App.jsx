import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store";

import Header from "./components/layout/Header";
import Home from "./components/pages/Home";
import SignUpPage from "./components/pages/SignUpPage";
import SignInPage from "./components/pages/SignInPage";
import Account from "./components/pages/Account";

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <>
            <Header />
            <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/signup" component={SignUpPage} />
              <Route path="/signin" component={SignInPage} />
              <Route path="/account" component={Account} />
            </Switch>
          </>
        </Router>
      </Provider>
    );
  }
}
