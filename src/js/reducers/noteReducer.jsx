import {
  ADD_NOTE_SUCCESS,
  ADD_NOTE_ERROR,
  DELETE_NOTE_SUCCESS,
  DELETE_NOTE_ERROR,
  FILTER
} from "../actions/types";

const initialState = {
  noteError: null,
  filter: ""
};

const noteReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_NOTE_SUCCESS: {
      return {
        ...state,
        noteError: null
      };
    }
    case ADD_NOTE_ERROR: {
      return {
        ...state,
        noteError: action.payload
      };
    }
    case DELETE_NOTE_SUCCESS: {
      return {
        ...state,
        noteError: null
      };
    }
    case DELETE_NOTE_ERROR: {
      return {
        ...state,
        noteError: action.payload
      };
    }
    case FILTER: {
      return {
        ...state,
        filter: action.payload
      };
    }
    default: {
      return state;
    }
  }
};

export default noteReducer;
