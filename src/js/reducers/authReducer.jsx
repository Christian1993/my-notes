import {
  SIGNIN_SUCCESS,
  SIGNIN_ERROR,
  SIGNOUT_SUCCESS,
  SIGNUP_SUCCESS,
  SIGNUP_ERROR,
  DELETE_ACCOUNT
} from "../actions/types";

const initialState = {
  authError: null
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case SIGNIN_SUCCESS: {
      return {
        ...state,
        authError: null
      };
    }
    case SIGNIN_ERROR: {
      return {
        ...state,
        authError: action.payload
      };
    }
    case SIGNOUT_SUCCESS: {
      return state;
    }
    case SIGNUP_SUCCESS: {
      return state;
    }
    case SIGNUP_ERROR: {
      return {
        ...state,
        authError: action.payload
      };
    }
    case DELETE_ACCOUNT: {
      return state;
    }
    default: {
      return state;
    }
  }
};

export default authReducer;
