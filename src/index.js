import React from 'react';
import ReactDOM from 'react-dom';
import App from './js/App';
import store from "./js/store"

import 'materialize-css/dist/css/materialize.min.css'
import './sass/main.sass';
import {
  materializeInit
} from './js/materialize/index.js'

store.firebaseAuthIsReady.then(() => {
    ReactDOM.render( < App / > , document.getElementById('root'));
  })
  .then(() => {
    materializeInit();
  })
