# My notes

#### Description:
 Application allows user create new account (one can also delete it, however all notes will be lost). Signed in users can add personal notes using dynamic forms. Notes are stored in firestore database. Firebase auth module was used to manage user state. UI was buit with the help of materialize.

#### Technologies used:
* react,
* redux,
* firebase (firestore, auth),
* sass,
* materialize

<img src="https://drive.google.com/uc?export=view&id=1VXvWuhk_RJ0B0L7c6dMZQzlM53gGBNv-" width="500" height="auto">
